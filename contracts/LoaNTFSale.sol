// SPDX-License-Identifier: MIT
pragma solidity 0.8.0;

contract LoaNTFSale {
    uint private constant sixteen_decimals_value = 10_000_000_000_000_000;

    // Item 1 = Immortal Skin
    // Item 2 = Genesis Capsule
    // Item 3 = Alpha Capsule
    // Item 4 = Beta Capsule

    mapping(uint => string) public _itemDescription;     // item => description
    mapping(uint => uint) public _itemTotalSupply;       // item => totalSupply
    mapping(uint => uint) public _itemPrice;             // item => price
    mapping(uint => uint) public _itemDateFrom;          // item => dateFrom
    mapping(uint => uint) public _itemDateTo;            // item => dateTo
    mapping(uint => mapping(address => uint)) public _itemOwned; // item => address => qty

    address public _admin; // Admin address

    // Deposit address variables
    uint public _depositAddressFinalizedDate;
    uint public _depositAddressesNumber;  // Number of deposit addresses
    uint public _startDepositAddressIndex;  // start ID of deposit addresses list
    mapping(uint => address) public _depositAddresses; // Deposit addresses
    mapping(address => bool) public _depositAddressesStatus; // Deposit addresses' whitelist status

    constructor() {
        _admin = msg.sender;

        _itemDescription[0] = 'Immortal Skin';
        _itemDescription[1] = 'Genesis Capsule';
        _itemDescription[2] = 'Alpha Capsule';
        _itemDescription[3] = 'Beta Capsule';

        _itemTotalSupply[0] = 50;
        _itemTotalSupply[1] = 1_000;
        _itemTotalSupply[2] = 5_100;
        _itemTotalSupply[3] = 70_000;

        _itemPrice[0] = 16 * 100 * sixteen_decimals_value; // 16
        _itemPrice[1] = 16 * 10  * sixteen_decimals_value; // 1.6
        _itemPrice[2] = 5  * 10  * sixteen_decimals_value; // 0.5
        _itemPrice[3] = 5  * sixteen_decimals_value;       // 0.05

        _itemDateFrom[0] = 1638489600; // 3/12/2021
        _itemDateFrom[1] = 1638662400; // 5/12/2021
        _itemDateFrom[2] = 1638835200; // 7/12/2021
        _itemDateFrom[3] = 1639008000; // 9/12/2021

        _itemDateTo[0] = 1638576000; // 4/12/2021
        _itemDateTo[1] = 1638748800; // 6/12/2021
        _itemDateTo[2] = 1638921600; // 8/12/2021
        _itemDateTo[3] = 1639094400; // 10/12/2021

        _depositAddressFinalizedDate = _itemDateFrom[0];
    }

    // Modifier
    modifier onlyAdmin() {
        require(_admin == msg.sender);
        _;
    }

    // Transfer ownership
    function transferOwnership(address payable admin) external onlyAdmin {
        require(admin != address(0), "Zero address");
        _admin = admin;
    }

    // Add deposit addresses and whitelist them
    function addDepositAddress(address[] calldata depositAddresses) external onlyAdmin {
        require(block.timestamp <_depositAddressFinalizedDate, "Unable to add address after the finalized date");
        uint depositAddressesNumber = _depositAddressesNumber;
        for (uint i = 0; i < depositAddresses.length; i++) {
            require(!_isContract(depositAddresses[i]), 'Contracts are not allowed');
            if (!_depositAddressesStatus[depositAddresses[i]]) {
                _depositAddresses[depositAddressesNumber] = depositAddresses[i];
                _depositAddressesStatus[depositAddresses[i]] = true;
                depositAddressesNumber++;
            }
        }
        _depositAddressesNumber = depositAddressesNumber;
    }

    // Remove deposit addresses and unwhitelist them
    // number - number of addresses to process at once
    function removeAllDepositAddress(uint number) external onlyAdmin {
        require(block.timestamp < _depositAddressFinalizedDate, "Unable to add address after the finalized date");
        uint i = _startDepositAddressIndex;
        uint last = i + number;
        if (last > _depositAddressesNumber) last = _depositAddressesNumber;
        for (; i < last; i++) {
            _depositAddressesStatus[_depositAddresses[i]] = false;
            _depositAddresses[i] = address(0);
        }
        _startDepositAddressIndex = i;
    }

    function buyImmortalSkin(uint qty) payable external {
        return _buyItem(0, qty);
    }

    function buyGenesisCapture(uint qty) payable external {
        return _buyItem(1, qty);
    }

    function buyAlphaCapture(uint qty) payable external {
        return _buyItem(2, qty);
    }

    function buyBetaCapture(uint qty) payable external {
        return _buyItem(3, qty);
    }

    function _buyItem(uint item, uint qty) internal {
        // require(item <= 3, 'Item should be 0, 1, 2 or 3');
        // require(qty > 0, 'Qty should be greater than 0');
        require(block.timestamp >= _itemDateFrom[item], 'Date has not yet started');
        require(block.timestamp <= _itemDateTo[item], 'Date has ended');
        require(
             _depositAddressesStatus[msg.sender] ||
            !_depositAddressesStatus[msg.sender] && block.timestamp >= (_itemDateFrom[item] + 600),
            'Non whitelist address can only start after 10 minutes from the start date'
        );
        require(qty <= _itemTotalSupply[item], 'Not enough supply to buy');
        require(_itemPrice[item] * qty == msg.value, 'Deposited amount should be the multiplier of qty');
        _itemOwned[item][msg.sender] = _itemOwned[item][msg.sender] + qty;
        _itemTotalSupply[item] = _itemTotalSupply[item] - qty;
    }

    // Allow admin to withdraw all the deposited BNB
    function withdrawAll() external onlyAdmin {
        payable(_admin).transfer(address(this).balance);
    }

    // Reject all direct deposit
    receive() external payable {
        revert();
    }

    // Local function
    function _isContract(address addr) internal view returns (bool) { uint size;
        assembly {
            size := extcodesize(addr)
        }
        return size > 0;
    }
}
